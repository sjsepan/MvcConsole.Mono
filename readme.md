# readme.md - README for MvcConsole.Mono

## About

Desktop console app demo, on Linux(/Win/Mac/GhostBSD), in C# Mono; requires mvclibrary.mono, ssepan.ui.mono.terminal, ssepan.application.mono, ssepan.io.mono, ssepan.utility.mono

### Purpose

To serve as a reference architecture and template for console and winforms apps that share a common model and settings file.
Also demonstrates my interpretation of the Model-View-Controller pattern.

![MvcConsole.Mono.png](./MvcConsole.Mono.png?raw=true "Screenshot")

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Mono knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Mono. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Mono project in the Sample folder.

### History

0.4:
~Bring command-line handling into parity with .Net Core apps

0.3:
~Initial release for Mono, cloned from *.Core project

### Fixes

~N/A

### Known Issues

~

Steve Sepan
<sjsepan@yahoo.com>
6/8/2024
