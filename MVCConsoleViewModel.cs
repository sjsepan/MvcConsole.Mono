﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
// using System.Collections.ObjectModel;
// using System.Diagnostics;
using System.Reflection;
using Ssepan.Utility.Mono;
using Ssepan.Ui.Mono.Terminal;
using Ssepan.Application.Mono;
using Ssepan.Io.Mono;
using MvcLibrary.Mono;

namespace MvcConsole.Mono
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCConsoleViewModel :
        ConsoleViewModel<string, MVCSettings, MVCModel>
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        public MVCConsoleViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCConsoleViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, string> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
                ModelController<MVCModel>.Model.SomeInt += 1;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt += 1;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt += 1;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("\nDid something.");
            }
        }
        #endregion Methods

    }
}
