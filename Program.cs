﻿using System;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Application.Mono;
using Ssepan.Utility.Mono;

namespace MvcConsole.Mono
{
    /*static*/ class Program
    {
        #region Declarations
        public const string APP_NAME = "MvcConsole.Mono";
        #endregion Declarations

        #region INotifyPropertyChanged
        public static event PropertyChangedEventHandler PropertyChanged;
        public static void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(null, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region PropertyChangedEventHandlerDelegate
        /// <summary>
        /// Note: property changes update UI manually.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
               if (e.PropertyName == "Filename")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Filename:{0}", Filename));
                }
                else if (e.PropertyName == "Directory")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Directory:{0}", Directory));
                }
                else if (e.PropertyName == "Format")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Format:{0}", Format));
                }
                else
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("{0}", e.PropertyName));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion PropertyChangedEventHandlerDelegate

        #region Properties
        private static string _Filename = default(string);
        public static string Filename
        {
            get { return _Filename; }
            set
            {
                _Filename = value;
                OnPropertyChanged(nameof(Filename));
            }
        }

        private static string _Directory;
        public static string Directory
        {
            get { return _Directory; }
            set
            {
                _Directory = value;
                OnPropertyChanged(nameof(Directory));
            }
        }

        private static string _Format; //json or xml
        public static string Format
        {
            get { return _Format; }
            set
            {
                _Format = value;
                OnPropertyChanged(nameof(Format));
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>int</returns>
        [STAThread]
        static int Main(string[] args)
        {
            //default to fail code
            int returnValue = -1;

            try
            {
                //define default output delegate
                ConsoleApplication.defaultOutputDelegate = ConsoleApplication.writeLineWrapperOutputDelegate;

                //subscribe to notifications
                PropertyChanged += PropertyChangedEventHandlerDelegate;

                //load, parse, run switches
                DoSwitches(args);

                returnValue = new ConsoleView()._Main();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                Console.WriteLine(string.Format("{0} did NOT complete: '{1}'", APP_NAME, ex.Message));
            }
            finally
            {
#if debug
                Console.Write("Press ENTER to continue> ");
#endif
                Console.ReadLine();
            }
            return returnValue;
        }

        #region ConsoleAppBase
        /// <summary>
        /// Note: switches are processed before Model or Settings are accessed.
        /// </summary>
        /// <param name="args"></param>
        static void DoSwitches(string[] args)
        {
            //define supported switches
            // -t -f:"filename" -h
            ConsoleApplication.DoCommandLineSwitches
            (
                args,
                new CommandLineSwitch[]
                {
                    new CommandLineSwitch("t", "t tests switch.", true, t),
                    new CommandLineSwitch("f", "f filename; overrides app.config", true, f)//,
                    //new CommandLineSwitch("H", "H invokes the Help command.", false, ConsoleApplication.Help)//may already be loaded
                }
            );
            //Note: switches are processed before Model or Settings are accessed.
        }

        //Note:model, Settings init done in viewmodel (after default handler set)
        #endregion ConsoleAppBase

        #region CommandLineSwitch Action Delegates
        /// <summary>
        /// Instance of an action conforming to delegate Action(Of TSettings), where TSettings is string.
        /// Command 't' tests the use of parameters.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="outputDelegate"></param>
        static private void t(string value, Action<string> outputDelegate)
        {
            try
            {
                outputDelegate(string.Format("t:\t{0}", value));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Validate and set selected settings.
        /// Instance of an action conforming to delegate Action<T>, where T is string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="outputDelegate"></param>
        static void f(string value, Action<string> outputDelegate)
        {
            try
            {
#if debug
                outputDelegate(string.Format("s{0}\t{1}", ConsoleApplication.CommandLineSwitchValueSeparator, value));
#endif

                //validate settings file path
                if (!System.IO.File.Exists(value))
                {
                    throw new ArgumentException(string.Format("Invalid settings file path: '{0}'", value));
                }
                Filename = value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion CommandLineSwitch Action Delegates
        #endregion Methods
    }
}
